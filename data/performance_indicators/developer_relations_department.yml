- name: Unique Wider Community Contributors per Month
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: Unique Wider Community Contributors based on merged contribution by month. Contributors are unique and only counted once for multiple MRs from the same contributor. (To view the dashboard your browser must allow third-party cookies)
  target: Above 170 contributors per month
  org: Developer Relations Department
  is_key: true
  dri: <a href="https://gitlab.com/nick_vh">Nick Veenhof</a>
  health:
    level: 2
    reasons:
      - Seeing an increase of returning contributors
      - MR Backlog decreased ~8.2% during the 2022-12-01 <> 2023-01-26
      - All time high of 466 review activity from GitLab Team Members to Wider Community in December 
      - All time high of 88 review activity between the Wider Community in December     
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/WiderCommunityPIsPart1/UniqueWiderCommunityContributorsperMonth

- name: Wider Community merged MRs per release
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: The Wider community contributions per milestone metric reflects the
    number of merge requests (MRs) merged from the wider GitLab community for each
    milestone from our GitLab.com data. To count as a contribution the MR must have
    been merged, have the <a href="https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution">Community
    contribution</a> label, have a GitLab milestone set (e.g <em>11.11</em>, <em>12.0</em>, etc.)
    and be against one of the <a href="https://about.gitlab.com/handbook/marketing/developer-relations/code-contributor-program/#monitored-projects">monitored
    gitlab-org group projects</a>.<br><br> Wider community metrics are more reliable
    after 2015 when the Community contribution label was created, and lastly, there
    is ongoing work to provide deeper insights into this metric in the <a href="https://10az.online.tableau.com/t/gitlab/views/WiderCommunityDashboard/WiderCommunityDashboard">Wider
    Community Tableau dashboard</a>. Further time
    period representations are provided on the <a href="https://10az.online.tableau.com/t/gitlab/views/BitergiaDashboard/BitergiaDashboard">Contributions and contributors over time dashboard</a> (To view the dashboard your browser must allow third-party cookies)
  target: 
  org: Developer Relations Department
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Under target, but showing steady increasing trend, amplified by the collaboration with the Quality Department.
    - Reached baseline for making current contribution process more effective. Ongoing longer term focus on improving the overall contributor journey.
  lessons:
    learned:
    - There is a seasonality to wider community contributions (e.g. lower activities
      during holiday periods).
    - The GitLab Hackathon has a direct impact in increasing a release's contributions.
    - Contributions to the www-gitlab-com repository (website and handbook) are generally not represented due to them not having a target milestone.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/WiderCommunityDashboard/WiderCommunitymergedMRsperrelease

- name: Wider Community merged MRs per month
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: The Wider community contributions per month metric reflects the
    number of merge requests (MRs) merged from the wider GitLab community for each
    month from our GitLab.com data. To count as a contribution the MR must have
    been merged, have the <a href="https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution">Community
    contribution</a> label
    and be against one of the <a href="https://about.gitlab.com/handbook/marketing/developer-relations/code-contributor-program/#monitored-projects">monitored
    gitlab-org group projects</a>.<br><br> Wider community metrics are more reliable after 2015 when the Community contribution label was created.
    Further time period representations are provided on the <a href="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/BitergiaDashboard/BitergiaDashboard">Contributions and contributors over time dashboard</a>
    (To view the dashboard your browser must allow third-party cookies) <br><br>
    <br/>
  target:
  org: Developer Relations Department
  is_key: false
  public: true
  health:
    level: 2
    reasons:
    - Under target, but showing steady increasing trend, amplified by the collaboration with the Quality Department.
    - Reached baseline for making current contribution process more effective. Ongoing longer term focus on improving the overall contributor journey.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/BitergiaDashboard/MRContributionMerged
  lessons:
    learned:
    - There is a seasonality to wider community contributions (e.g. lower activities
      during holiday periods).
    - The GitLab Hackathon has a direct impact in increasing a contributions on a given month and the next.


- name: First time contributors per month
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: The First time contributors per month metric reflects the
    number of wider GitLab community members that contribute for the first time to GitLab on a given month.
    To count as a first time contributor, their MR must have
    been merged, have the <a href="https://about.gitlab.com/handbook/marketing/developer-relations/code-contributor-program/#contribution-labels">1st-time-contributor label</a>
    and be against one of the <a href="https://about.gitlab.com/handbook/marketing/developer-relations/code-contributor-program/#monitored-projects">monitored
    gitlab-org group projects</a>.<br><br> Wider community metrics are more reliable
    after 2018 when the 1st-time-contributor label was created. 
    Further time period representations are provided on the <a href="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/BitergiaDashboard/BitergiaDashboard">Contributions and contributors over time dashboard</a>
     <br><br>
    <br/>
    <strong>Note</strong> - if you cannot see this chart, please <a href="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/BitergiaDashboard/FirstTimeContributors">go to the original Tableau chart</a>. (To view the dashboard your browser must allow third-party cookies)
  target: 
  org: Developer Relations Department
  is_key: false
  public: true
  health:
    level: 1
    reasons:
    - Unknown at this time, further research is required.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/BitergiaDashboard/FirstTimeContributors

- name: Developer Relations Monthly Outreach 
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: The Developer Relations team aims to build thought leadership accross several mediums, primarily through content while exploring various other means. We track the reach of content generated to understand it's impact. We track pageviews of blog posts authored by the Developer Relations team, views of YouTube videos uploaded by our team members, and the reach of our social media shares.(To view the dashboard your browser must allow third-party cookies)
  target: 
  org: Developer Relations Department
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - This is a new way of measuring this metric and we are still learning about the best way to track these numbers. 
- name: Active Community Members
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: The Developer Relations team seeks to grow engagement with our community across a number of channels. We measure this activity through the "Active Community Members" metric in Common Room. This metric reflects the number of people actively posting, commenting, or engaging with GitLab content across a number of channels including GitLab.com. 
  target: 
  org: Developer Relations Department
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - This is a new way of measuring this metric and we are still learning about the best way to track these numbers.     
- name: GitLab for Education Quarterly Active Seats
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The GitLab for Education team aims to facilitate and drive adoption of GitLab at educational institutions around the world and build an engaged community of GitLab Evangelists in the next generation of the workforce. We track the number of active seats in the GitLab for Education Program on a quarterly basis as a gauge of program enrollment and rentention. The number of active seats is determied by the number of seats from New Business, Add-ons, and Renewals.
  target: 
  org: Developer Relations Department
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - All growth prior to FY22Q1 has been organic. Outreach efforts are underway to promote the program.
    - Ongoing longer term focus on outreach (new) and enablement for existing program members (renewals).
- name: GitLab for Education Quarterly New Institutions
  base_path: "/handbook/marketing/performance-indicators/"
  parent: "/handbook/marketing/performance-indicators/#gitlab-for-education-quarterly-active-seats"
  definition: We track the number of new intitutions joining the GitLab for Education Program on a quarterly basis. The number of new institutions is a gauge for program awareness and expansion.
  target: 
  org: Developer Relations Department
  is_key: false
  public: true
  health:
    level: 2
    reasons:
      - All growth prior to FY22Q1 has been organic. Outreach efforts are underway to promote the program.
- name: GitLab for Education Quarterly New Seats
  base_path: "/handbook/marketing/performance-indicators/"
  parent: "/handbook/marketing/performance-indicators/#gitlab-for-education-quarterly-active-seats"
  definition: We track the number of new seats in GitLab for Education Program on a quarterly basis. The number of new seats is a gauge for GitLab adoption.
  target: 
  org: Developer Relations Department
  is_key: false
  public: true
  health:
    level: 2
    reasons:
      - All growth prior to FY22Q1 has been organic. Outreach efforts are underway to promote the program.
- name: GitLab Community Forum Likes Per Quarter
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The Developer Relations team supports the GitLab Community Forum as an official place for the wider community to seek technical support and assistance with GitLab. We track the average number of likes on comments across the forum over the quarter as a measure of helpful comments and impressions. We are also exploring other ways to track forum success.
  target: 
  org: Developer Relations Department
  is_key: false
  public: true
  health:
    level: 3
    reasons:
    - This metric is new and is performing at the same threshold as last year.

############################
# Contributor Success
############################

- name: MRARR
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: MRARR (pronounced "mer-arr," like a pirate) is the measurement of Merge Requests from customers, multiplied with
    the revenue of the account (ARR) from that customer. This measures how active our biggest customers are contributing to GitLab.
    We believe the higher this number the better we'll retain these customers and improve product fit for large enterprises.
    The unit of MRARR is MR Dollars (MR$). MR Dollars is different than the normal Dollars which is used for ARR.
    We are tracking current initiatives to improve this in this <a href="https://gitlab.com/groups/gitlab-com/-/epics/1225">epic</a>. <figure class="video_container"> <iframe src="https://www.youtube.com/embed/HjWwpPU3KDU" frameborder="0" allowfullscreen="true"> </iframe></figure>
  target: Identified in SiSense Chart
  org: Developer Relations Department
  is_key: true
  dri: <a href="https://gitlab.com/nick_vh">Nick Veenhof</a>
  health:
    level: 0
    reasons:
      - Trending up and stabilizing
      - Automated identification of Leading Organizations and Review Time SLO implemented
      - Switched effort to on-board new contributing customers
      - Customer contributions are now being identified
  urls:
    - https://10az.online.tableau.com/t/gitlab/views/MRARRDashboard_17055242209630/MRARRDashboard

- name: Open Community MR Age
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: OCMA (pronounced "ock-mah") measures the median time of all open MRs as of a specific date.
    In other words, on any given day, this calculates the number of open MRs and median time in an open state for open MRs at that point in time. (To view the dashboard your browser must allow third-party cookies)
  target: Below 100 days
  org: Developer Relations Department
  is_key: false
  health:
    level: 2
    reasons:
      - Above target. Our backlog of open MRs is decreasing at a steady rate
      - Some stages/groups hold more than 25% of the open wider community MRs
      - Collaborating with Runner team to address old community contribution
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/WiderCommunityPIsPart1/OCMA
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12325

- name: Leading Organizations MR Time-to-review
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: Leading Organizations are entitled to a Review-Response SLO of 4 working days. (To view the dashboard your browser must allow third-party cookies)
  target: Below or equal to 4 working days
  org: Developer Relations Department
  is_key: false
  health:
    level: 3
    reasons:
      - Above target at 5 working days
      - Holiday break caused a spike that is visible in January.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/LeadingOrganizationsPIsPart1/LeadingOrganizationsTimetoReviewMonthly
  urls:
    - https://about.gitlab.com/handbook/engineering/workflow/code-review/#review-response-slo

- name: Returning vs new contributors
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: Returning vs new contributors. Having more returning contributors, in combination with a growing amount of unique wider community members, is a sign of a healthy contributor community.(To view the dashboard your browser must allow third-party cookies)
  target: TBD
  org: Developer Relations Department
  is_key: false
  health:
    level: 2
    reasons:
      - Slightly under target. At 56% for current month
      - Low total amount of new contributors since September `22
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/WiderCommunityPIsPart1/NewvsReturningMonthlyCommunityContributors
  urls:
    - https://about.gitlab.com/handbook/engineering/workflow/code-review/#review-response-slo

- name: Community MR Coaches per Month
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: The number of MR Coaches defined by team.yml role (To view the dashboard your browser must allow third-party cookies)
  target: Above 50 coaches per month
  org: Developer Relations Department
  is_key: false
  health:
    level: 2
    reasons:
      - Increased to 40
      - Improved automation and more self-service bot commands allow MR Coaches to focus on the more difficult cases
      - Contributor Success team expanded, and every member of that team is an MR coaches by default
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/WiderCommunityPIsPart2/CommunityMRCoachesperMonth
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10519

- name: Feature Community Contribution MRs
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: Percentage of Community Contributions that are related to feature development (To view the dashboard your browser must allow third-party cookies)
  target: Above 20%
  org: Developer Relations Department
  is_key: false
  health:
    level: 3
    reasons:
      - Above target for 5 consecutive months
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/WiderCommunityPIsPart2/CommunitycontributionMRsaddedasfeaturespermonth

- name: Community MR Percentage
  base_path: "/handbook/marketing/developer-relations/performance-indicators/"
  definition: This is the ratio of community contributions with the number of merged product MRs. As we grow as a company we want to make sure the community scales with the company. (To view the dashboard your browser must allow third-party cookies)
  target: Above 8% of all MRs
  org: Developer Relations Department
  is_key: false
  health:
    level: 2
    reasons:
      - Percentage went down to 5%
      - The Wider Community is not growing as fast as the increase of GitLab team members
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/WiderCommunityPIsPart2/PercentofMRsfromCommunity

