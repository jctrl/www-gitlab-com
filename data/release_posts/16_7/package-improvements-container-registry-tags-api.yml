---
features:
  secondary:
  - name: "List repository tags with new Container Registry API"
    available_in: [free, silver, gold]
    documentation_link: 'https://docs.gitlab.com/ee/api/container_registry.html#list-registry-repository-tags'
    reporter: trizzi
    stage: package
    categories:
    - 'Container Registry'
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/411387'
    description: |
      Previously, the Container Registry relied on the Docker/OCI [listing image tags registry API](https://gitlab.com/gitlab-org/container-registry/-/blob/5208a0ce1600b535e529cd857c842fda6d19ad59/docs/spec/docker/v2/api.md#listing-image-tags) to list and display tags in GitLab. This API had significant performance and discoverability limitations.

      This API performed slowly because the number of network requests against the registry scaled with the number of tags in the tags list. In addition, because the API didn't track publish time, the published timestamp was often incorrect. There were also limitations when displaying images based on Docker manifest lists or OCI indexes, such as for multi-architecture images.
      
      To address these limitations, we introduced a new registry [list repository tags API](https://gitlab.com/gitlab-org/container-registry/-/blob/5208a0ce1600b535e529cd857c842fda6d19ad59/docs/spec/gitlab/api.md#list-repository-tags). By updating the user interface to use the new API, the number of requests to the Container Registry is reduced to just one. Publish timestamps are also accurate, and there is more robust support for multi-architecture images.

      This feature is available only on GitLab.com. Self-managed support is blocked until the next-generation Container Registry is generally available. To learn more, see [issue 423459](https://gitlab.com/gitlab-org/gitlab/-/issues/423459).
