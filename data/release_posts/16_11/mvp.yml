# Release post data file for MVP item
#
# Read through the Release Posts Handbook for more information:
# https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp
#
# DELETE THESE COMMENTS BEFORE SUBMITTING THE MERGE REQUEST
---
mvp:
  fullname: ['Ivan Shtyrliaiev', 'Baptiste Lalanne']
  gitlab: ['bahek2462774', 'BaptisteLalanne']
  description: |
    [Ivan Shtyrliaiev](https://gitlab.com/bahek2462774) has made [half a dozen contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=merged&author_username=bahek2462774) to GitLab so far in 2024.
    He was nominated by [Hannah Sutor](https://gitlab.com/hsutor), Principal Product Manager at GitLab, who highlighted his contribution to [improve the Users list search and filter experience](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144907).

    “This is a huge user experience improvement that helps us go from a horizontally scrollable list of tabs to a much more elegant UX with only 2 tabs and a search box,” Hannah said.
    “Now users are able to filter down via the search box rather than horizontally scroll tabs!”

    Ivan was noted for picking up this challenging request, working with the GitLab UX team to refine the proposal, and being super responsive to reviews.
    [Adil Farrukh](https://gitlab.com/adil.farrukh), Engineering Manager at GitLab, supported the nomination, noting that this feature was not trivial and that Ivan was very responsive to feedback.
    [Eduardo Sanz García](https://gitlab.com/eduardosanz), Sr. Frontend Engineer at GitLab, also supported the nomination and commended Ivan's resilience.

    "Really appreciate Eduardo's review and the GitLab team putting in so much effort to make contributions happen," Ivan said. 
    "It was very helpful and I realise how much time it takes."
    
    Ivan is a frontend software engineer at [Politico](https://www.politico.com/).

    [Baptiste Lalanne](https://gitlab.com/BaptisteLalanne) picked up a three-year-old issue with nearly seventy upvotes to contribute a [highly requested feature](https://gitlab.com/gitlab-org/gitlab/-/issues/262674) that adds `retry:exit codes` to the CI/CD configuration.
    This contribution empowers our users with enhanced flexibility in managing failed pipeline jobs and jobs with different exit codes.

    Baptiste was nominated by [Dov Hershkovitch](https://gitlab.com/dhershkovitch), Product Manager at GitLab.
    “Baptiste's diligent work on this project went above and beyond mere implementation,” Dov said.
    “This accomplishment serves as a prime example of our community's collaborative strength.
    Through Baptiste's efforts, GitLab has not only fulfilled a critical need but also reinforced its commitment to openness and transparency, enriching our open-core mentality.”

    "This is heart warming and really appreciated," Baptiste said. 
    "I'm really looking forward to continuing my contributions in my spare time as I love it so much."

    Over the past year, Baptiste has merged six merge requests to GitLab and is looking to [contribute to the GitLab Runner](https://docs.gitlab.com/runner/development/) next.
    Baptiste is a software engineer for [DataDog](https://www.datadoghq.com/).

    A big thanks to our newest MVPs, Ivan and Baptiste, and to the rest of GitLab's community contributors! 🙌
