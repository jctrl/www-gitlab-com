---
layout: markdown_page
title: "Group Direction - Secret Detection"
description: "GitLab's goal is to provide Secret Detection as part of the standard development process. Learn more!"
canonical_path: "/direction/secure/secret-detection/"
---

- TOC
{:toc}

## Secret Detection

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Content Last Reviewed | `2024-04-19` |
| Content Last Updated  | `2024-04-19` |

## Overview

Secret Detection is a group in the Secure stage. There are two categories in the group and details on the direction can be viewed on the following individual category pages:

1. [Secret Detection](https://about.gitlab.com/direction/secure/secret-detection/secret-detection/)
1. [Code Quality](https://about.gitlab.com/direction/secure/secret-detection/code-quality)

## Why our group exists

We believe that the world is safer when [everyone can contribute](https://handbook.gitlab.com/handbook/company/mission/#mission) to software security.
Our customers, and those they serve, are better protected when developers and security professionals can fix potential security risks earlier.

The earliest possible time to catch a security issue is when the code is first written.
GitLab sees code very early in the software development lifecycle, since we store production code and also support customer workflows (like merge requests) for pre-production development.
So, our group is uniquely positioned to integrate static analysis everywhere as part of a comprehensive DevSecOps platform.
We can do what others can't by making security omnipresent, and by supporting collaboration right in the tools that development teams are already using to do their jobs.

Building on those fundamental beliefs, the Secret Detection group's _business_ purpose is to build value for GitLab and our customers...
1. By encouraging organizations to use GitLab Ultimate...
1. By delivering more value to them in the GitLab platform...
1. By enabling them to secure their code early in the development process...

## Our responsibility

We are responsible for ensuring that customers can use GitLab Ultimate to:
- Remove existing security “point solutions”, with a special focus on Secret Detection and Code Quality (Customers often use the savings from removing other tools to pay for GitLab Ultimate).
- Satisfy their security and compliance requirements relevant to these areas.
- Meaningfully increase their confidence in the security of their code as soon as it is written.

Our responsibility is for the **full customer experience**—not just security analyzers or specific software systems we maintain. At times this may mean:
- Imagining and implementing workflows that helps everyone know about and fix security issues.
- Integrating open-source tools into GitLab.
- Creating innovative static analysis technology.
- Driving changes in other areas of the GitLab platform.
- Contributing to documentation or other educational material that helps explain how to use GitLab effectively.

**We will do what it takes** to deliver these customer results—our customers use the entire product to do their jobs, so it's important that we collaborate effectively with other groups to deliver end-to-end results.


## Priorities

| Priority | Name | Target release |
| -------- | ---- | -------------- | 
<% data.product_priorities.secret_detection.priorities.each.with_index do |priority, index| %>
| <%= index + 1 %> | [<%= priority.name %>](<%= priority.url %>) | `<%= priority.target_release.presence || "TBD" %>`  |
<% end %>

### UX initiatives

| Name | Rationale | Needs | Status/progress |
| ---- | --------- | ----- | --------------- |
<% data.product_priorities.secret_detection.ux_priorities.each.with_index do |priority, index| %>
| [<%= priority.name %>](<%= priority.url %>) | <%= priority.rationale %> | <%= priority.needs %> | <%= priority.status %> |
<% end %>

*This page may contain information related to upcoming products, features and functionality.

It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.

Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.*
